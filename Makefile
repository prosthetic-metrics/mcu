CC = avr-gcc
CFLAGS = -mmcu=atmega328p -std=c99 -Wall -Wextra -Wfloat-equal -Wundef -Wshadow -Wpointer-arith -Wcast-align -Wstrict-prototypes -Wmissing-prototypes -Wstrict-overflow=5 -Wwrite-strings -Waggregate-return -Wcast-qual -Wswitch-default -Wswitch-enum -Wconversion -Wunreachable-code -Wno-unused-result -Werror-implicit-function-declaration -pedantic -ftrapv -Os
PRGRMR = avrdude
PRGRMR_FLAGS = -p m328p -c arduino -P /dev/ttyACM0

RM = rm

SRCS = $(shell ls *.c)
OBJS = $(subst .c,.c.out,$(SRCS))


all: compile

compile: $(SRCS)
	for src in $(SRCS) ; do \
		$(CC) $(CFLAGS) -o $$src.out $$src ; \
	done

clean:
	$(RM) $(OBJS)

install: $(OBJS)
	for obj in $(OBJS) ; do \
		$(PRGRMR) $(PRGRMR_FLAGS) -U flash:w:$$obj ; \
	done

uninstall:
	$(PRGRMR) $(PRGRMR_FLAGS) -e
